// Fill out your copyright notice in the Description page of Project Settings.


#include "SGSnakeElementBase.h"
#include "Components/StaticMeshComponent.h"
#include "SGSnakeBase.h"

ASGSnakeElementBase::ASGSnakeElementBase()
{
	PrimaryActorTick.bCanEverTick = true;
    
    StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
    StaticMesh->SetupAttachment(RootComponent);
    StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    
    StaticMesh->SetVisibility(false, true);
}

void ASGSnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	check(StaticMesh);
}

void ASGSnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASGSnakeElementBase::SetFirstElementType_Implementation()
{
    StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &ASGSnakeElementBase::OnComponentBeginOverlap);
}

void ASGSnakeElementBase::Interact(AActor* Interactor, bool IsHead)
{
    if (const auto SnakeActor = Cast<ASGSnakeBase>(Interactor))
    {
        SnakeActor->Destroy();
        UE_LOG(LogTemp, Display, TEXT("DESTROYED!"));
    }
    
}

void ASGSnakeElementBase::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, //
    AActor* OtherActor,                                                                     //
    UPrimitiveComponent* OtherComp,                                                         //
    int32 OtherBodyIndex,                                                                   //
    bool bFromSweep,                                                                        //
    const FHitResult& SweepResult)
{
    if (SnakeOwner)
    {
        SnakeOwner->SnakeElementOverlap(this, OtherActor);
    }
}

void ASGSnakeElementBase::ToggleCollision()
{
    if (StaticMesh->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
    {
        StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    }
    else
    {
        StaticMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    }
}
