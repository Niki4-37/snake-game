// Fill out your copyright notice in the Description page of Project Settings.


#include "SGFreezeHunger.h"
#include "SGSnakeBase.h"


ASGFreezeHunger::ASGFreezeHunger()
{
	PrimaryActorTick.bCanEverTick = false;

}

void ASGFreezeHunger::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASGFreezeHunger::Interact(AActor* Interactor, bool IsHead)
{
    if (!IsHead) return;

    const auto Snake = Cast<ASGSnakeBase>(Interactor);
    if (!Snake) return;

    Snake->FreezeHunger();
    
    Destroy();
}

