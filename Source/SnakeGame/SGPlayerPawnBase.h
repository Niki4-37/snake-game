// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SGPlayerPawnBase.generated.h"

class UCameraComponent;
class ASGSnakeBase;
class ASGFood;
class ASGFreezeHunger;

UCLASS()
class SNAKEGAME_API ASGPlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	ASGPlayerPawnBase();

protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera")
    UCameraComponent* PawnCamera;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SnakeConfig")
    TSubclassOf<ASGSnakeBase> SnakeClass;

    UPROPERTY()
    ASGSnakeBase* Snake = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Bonuses")
    TSubclassOf<ASGFood> BonusFoodClass;

    UPROPERTY()
    ASGFood* BonusFood = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bonuses")
    TSubclassOf<ASGFreezeHunger> BonusFreezeHungerClass;

    UPROPERTY()
    ASGFreezeHunger* FreezeHunger = nullptr;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Bonuses", meta = (ClampMin = "1", ClampMax = "30"))
    float BonusCooldown = 10.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Playground", meta = (ClampMin = "2", ClampMax = "30"))
    int32 PlaygroundSize = 20;

	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION()
    void OnDestroyed(AActor* DestroyedActor);

    // For User Widget
    float GetEnergyPercentage();
    int32 GetScorePoints() const;

    int32 GetPlaygroundSize() const { return PlaygroundSize; };

private:
    UPROPERTY()
    FTimerHandle BonusSpawner;
    
    void CreateSnake();

    void HandlePlayerVerticalInput(float Amount);
    void HandlePlayerHorizontalInput(float Amount);

    /* //Second way Pawn InputComponent
    void SetDirection(int DirectionNum);
    */
    void InitSpawnFreezeHungerBonus();
    void SpawnFreezeBonus();

    void SpawnBonus(UClass* SpawnClass, AActor* Actor);
};
