// Fill out your copyright notice in the Description page of Project Settings.

#include "SGPlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SGSnakeBase.h"
#include "SGFood.h"
#include "SGFreezeHunger.h"
#include "Components/InputComponent.h"

ASGPlayerPawnBase::ASGPlayerPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

    PawnCamera = CreateDefaultSubobject<UCameraComponent>("PawnCamera");
    RootComponent = PawnCamera;
}

void ASGPlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90.f, 0, 0));
    check(GetWorld());
    
    CreateSnake();
    InitSpawnFreezeHungerBonus();
    SpawnBonus(BonusFoodClass, BonusFood);
}

void ASGPlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASGPlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

    // First way Pawn InputComponent
    PlayerInputComponent->BindAxis("Vertical", this, &ASGPlayerPawnBase::HandlePlayerVerticalInput);
    PlayerInputComponent->BindAxis("Horizontal", this, &ASGPlayerPawnBase::HandlePlayerHorizontalInput);

    /* //Second way Pawn InputComponent
    DECLARE_DELEGATE_OneParam(FMovementDirection, int);
    PlayerInputComponent->BindAction<FMovementDirection>("MoveUp", IE_Pressed, this, &ASGPlayerPawnBase::SetDirection, 0);
    PlayerInputComponent->BindAction<FMovementDirection>("MoveDown", IE_Pressed, this, &ASGPlayerPawnBase::SetDirection, 1);
    PlayerInputComponent->BindAction<FMovementDirection>("MoveLeft", IE_Pressed, this, &ASGPlayerPawnBase::SetDirection, 2);
    PlayerInputComponent->BindAction<FMovementDirection>("MoveRight", IE_Pressed, this, &ASGPlayerPawnBase::SetDirection, 3);
    */
}

void ASGPlayerPawnBase::OnDestroyed(AActor* DestroyedActor)
{
    if (Cast<ASGFood>(DestroyedActor))
    {
        SpawnBonus(BonusFoodClass, BonusFood);
    }
    else if (Cast<ASGFreezeHunger>(DestroyedActor))
    {
        InitSpawnFreezeHungerBonus();
    }
}

float ASGPlayerPawnBase::GetEnergyPercentage()
{
    if (!Snake) return 0.f;
    return Snake->GetEnergyPercentage();
}

int32 ASGPlayerPawnBase::GetScorePoints() const
{
    if (!Snake) return 0;
    return Snake->GetScorePoints();
}

void ASGPlayerPawnBase::CreateSnake()
{
    Snake = GetWorld()->SpawnActor<ASGSnakeBase>(SnakeClass, FTransform());
}
// First way Pawn InputComponent
void ASGPlayerPawnBase::HandlePlayerVerticalInput(float Amount)
{
    if (!Snake) return;
    if (Amount > 0 && Snake->SnakeMovementDirection != EMovementDirection::DOWN)
    {
        Snake->SnakeMovementDirection = EMovementDirection::UP;
    }
    else if (Amount < 0 && Snake->SnakeMovementDirection != EMovementDirection::UP)
    {
        Snake->SnakeMovementDirection = EMovementDirection::DOWN;
    }
}
// First way Pawn InputComponent
void ASGPlayerPawnBase::HandlePlayerHorizontalInput(float Amount)
{
    if (!Snake) return;
    if (Amount > 0 && Snake->SnakeMovementDirection != EMovementDirection::LEFT)
    {
        Snake->SnakeMovementDirection = EMovementDirection::RIGHT;
    }
    else if (Amount < 0 && Snake->SnakeMovementDirection != EMovementDirection::RIGHT)
    {
        Snake->SnakeMovementDirection = EMovementDirection::LEFT;
    }
}
/* //Second way Pawn InputComponent
void ASGPlayerPawnBase::SetDirection(int DirectionNum)
{
    Snake->SetDirection(DirectionNum);
}
*/
void ASGPlayerPawnBase::InitSpawnFreezeHungerBonus()
{
    GetWorld()->GetTimerManager().SetTimer(BonusSpawner, this, &ASGPlayerPawnBase::SpawnFreezeBonus, BonusCooldown, false);
}

void ASGPlayerPawnBase::SpawnFreezeBonus()
{
    SpawnBonus(BonusFreezeHungerClass, FreezeHunger);
    GetWorld()->GetTimerManager().ClearTimer(BonusSpawner);
}

void ASGPlayerPawnBase::SpawnBonus(UClass* SpawnClass, AActor* Actor)
{
    float RandXPosition = FMath::RandHelper(PlaygroundSize / 2) * Snake->GetDistanceBetween() * (FMath::RandBool() ? 1 : -1);
    float RandYPosition = FMath::RandHelper(PlaygroundSize / 2) * Snake->GetDistanceBetween() * (FMath::RandBool() ? 1 : -1);
    FVector FoodLocation = FVector(RandXPosition, RandYPosition, 0);
    FTransform SpawnTransform(FoodLocation);
    Actor = GetWorld()->SpawnActor<AActor>(SpawnClass, SpawnTransform);
    if (Actor)
    {
        Actor->OnDestroyed.AddDynamic(this, &ASGPlayerPawnBase::OnDestroyed);
    }
}

