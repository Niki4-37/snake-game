// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGInteractable.h"
#include "SGSnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASGSnakeBase;

UCLASS()
class SNAKEGAME_API ASGSnakeElementBase : public AActor, public ISGInteractable
{
	GENERATED_BODY()
	
public:	
	ASGSnakeElementBase();

protected:
	virtual void BeginPlay() override;
    
public:	
    UPROPERTY()
    ASGSnakeBase* SnakeOwner;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
    UStaticMeshComponent* StaticMesh;

	virtual void Tick(float DeltaTime) override;

    UFUNCTION(BlueprintNativeEvent)
    void SetFirstElementType();
    void SetFirstElementType_Implementation();

    virtual void Interact(AActor* Interactor, bool IsHead) override;

    UFUNCTION()
    void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent,  //
        AActor* OtherActor,                                                 //
        UPrimitiveComponent* OtherComp,                                     //
        int32 OtherBodyIndex,                                               //
        bool bFromSweep,                                                    //
        const FHitResult& SweepResult);

    void ToggleCollision();
};
