// Fill out your copyright notice in the Description page of Project Settings.


#include "SGHUD.h"
#include "Blueprint/UserWidget.h"

void ASGHUD::BeginPlay()
{
    Super::BeginPlay();

    auto UserWidget = CreateWidget<UUserWidget>(GetWorld(), UserWidgetClass);
    if (UserWidget)
    {
        UserWidget->AddToViewport();
    }
}
