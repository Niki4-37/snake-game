// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGSnakeBase.generated.h"

class ASGSnakeElementBase;

UENUM(BlueprintType)
enum class EMovementDirection : uint8
{
    UP,
    DOWN,
    LEFT,
    RIGHT
};

UCLASS()
class SNAKEGAME_API ASGSnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ASGSnakeBase();

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeConfig")
    float DistanceBetween = 100.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeConfig")
    float TickInterval = 1.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SnakeConfig")
    float MaxEnergy = 100.f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "SnakeConfig")
    float SecondsToFreezeHunger = 5.f;

    float Energy;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SnakeConfig")
    TSubclassOf<ASGSnakeElementBase> SnakeElementClass;
	
    UPROPERTY()
    TArray<ASGSnakeElementBase*> SnakeElements;

    /* //Set protected for second way Pawn InputComponent
    UPROPERTY(BlueprintReadOnly)
    EMovementDirection SnakeMovementDirection;
    */
    virtual void BeginPlay() override;

public:
    UPROPERTY(BlueprintReadOnly)
    EMovementDirection SnakeMovementDirection;
	
    virtual void Tick(float DeltaTime) override;

    void Move();
    
    /* //Second way Pawn InputComponent
    void SetDirection(int DirectionNum);
    */

    UFUNCTION()
    void SnakeElementOverlap(ASGSnakeElementBase* OvelappedBlock, AActor* OtherActor);

    void AddElement(int ElementsNum = 1);

    float GetDistanceBetween() const { return DistanceBetween; };
    float GetEnergyPercentage() const { return Energy / MaxEnergy; };

    void AddEnergy(float EnergyPoints = 10.f);

    void FreezeHunger();

    int32 GetScorePoints() const { return ScorePoints; };
    void AddScorePoints (int32 Points = 1);

private:
    bool bIsFreezed = false;
    FTimerHandle TimeToUnfreezHunger;
    void UnFreezeHunger();

    int32 ScorePoints = 0;

    void CheckPlaygroundBorders();
};
