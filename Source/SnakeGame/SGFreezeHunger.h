// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SGInteractable.h"
#include "SGFreezeHunger.generated.h"

UCLASS()
class SNAKEGAME_API ASGFreezeHunger : public AActor, public ISGInteractable
{
	GENERATED_BODY()
	
public:	
	ASGFreezeHunger();

protected:
	virtual void BeginPlay() override;

public:	
    virtual void Interact(AActor* Interactor, bool IsHead) override;
};
