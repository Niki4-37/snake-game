// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SGHUD.generated.h"

UCLASS()
class SNAKEGAME_API ASGHUD : public AHUD
{
	GENERATED_BODY()
	
protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Snake Info")
    TSubclassOf<UUserWidget> UserWidgetClass;

    virtual void BeginPlay() override;
    
};
