// Fill out your copyright notice in the Description page of Project Settings.


#include "SGFood.h"
#include "SGSnakeBase.h"

// Sets default values
ASGFood::ASGFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASGFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASGFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASGFood::Interact(AActor* Interactor, bool IsHead)
{
    if (!IsHead) return;
    
    const auto SnakeActor = Cast<ASGSnakeBase>(Interactor);
    if (!SnakeActor) return;
    
    SnakeActor->AddElement();
    SnakeActor->AddEnergy();
    SnakeActor->AddScorePoints();

    Destroy();
}

