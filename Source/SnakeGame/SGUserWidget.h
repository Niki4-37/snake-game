// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SGUserWidget.generated.h"

UCLASS()
class SNAKEGAME_API USGUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
    UFUNCTION(BlueprintCallable)
    float GetEnergyPercentage() const;

    UFUNCTION(BlueprintCallable)
    int32 GetScorePoints() const;
};
