// Fill out your copyright notice in the Description page of Project Settings.


#include "SGSnakeBase.h"
#include "SGSnakeElementBase.h"
#include "SGInteractable.h"
#include "SGPlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"

ASGSnakeBase::ASGSnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;

}

void ASGSnakeBase::BeginPlay()
{
	Super::BeginPlay();
	check(GetWorld());
    SetActorTickInterval(TickInterval);
    AddElement(4);
    SnakeMovementDirection = EMovementDirection::DOWN;
    Energy = MaxEnergy;
}

// Called every frame
void ASGSnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    Move();
}

void ASGSnakeBase::Move()
{
    FVector SnakeDirection(ForceInitToZero);

    switch (SnakeMovementDirection) 
    {
        case EMovementDirection::UP :
        {
            SnakeDirection.X += DistanceBetween;
            break;
        }
        case EMovementDirection::DOWN :
        {
            SnakeDirection.X -= DistanceBetween;
            break;
        }
        case EMovementDirection::LEFT :
        {
            SnakeDirection.Y -= DistanceBetween;
            break;
        }
        case EMovementDirection::RIGHT :
        {
            SnakeDirection.Y += DistanceBetween;
            break;
        }
    }

    SnakeElements[0]->ToggleCollision();

    for (int i = SnakeElements.Num() - 1; i > 0; --i)
    {
        auto CurrentElemet = SnakeElements[i];
        const auto PrevElement = SnakeElements[i - 1];
        CurrentElemet->SetActorLocation(PrevElement->GetActorLocation());
        
        if (!CurrentElemet->StaticMesh->GetVisibleFlag())
        {
            UE_LOG(LogTemp, Display, TEXT("Snake Element set visible"));
            CurrentElemet->StaticMesh->SetVisibility(true, true);
        }
    }
    
    SnakeElements[0]->AddActorWorldOffset(SnakeDirection);
    SnakeElements[0]->ToggleCollision();
    if (!SnakeElements[0]->StaticMesh->GetVisibleFlag())
    {
        UE_LOG(LogTemp, Display, TEXT("Snake Element set visible"));
        SnakeElements[0]->StaticMesh->SetVisibility(true, true);
    }

    if (!bIsFreezed)
    {
        --Energy;
    }
    
    if (FMath::IsNearlyZero(Energy))
    {
        Destroy();
    }

    CheckPlaygroundBorders();
}

void ASGSnakeBase::SnakeElementOverlap(ASGSnakeElementBase* OvelappedBlock, AActor* OtherActor)
{
    if (!OvelappedBlock) return;
    
    int32 ElementIndex;
    SnakeElements.Find(OvelappedBlock, ElementIndex);
    bool isHead = ElementIndex == 0;
    if (const auto InteractableInterface = Cast<ISGInteractable>(OtherActor))
    {
        InteractableInterface->Interact(this, isHead);
    }
}
/* //Second way Pawn InputComponent
void ASGSnakeBase::SetDirection(int DirectionNum)
{
    if (DirectionNum < 0 && DirectionNum > 4) return;
    else if (SnakeMovementDirection == EMovementDirection::DOWN && DirectionNum == 0) return;
    else if (SnakeMovementDirection == EMovementDirection::UP && DirectionNum == 1) return;
    else if (SnakeMovementDirection == EMovementDirection::RIGHT && DirectionNum == 2) return;
    else if (SnakeMovementDirection == EMovementDirection::LEFT && DirectionNum == 3) return;
    
    SnakeMovementDirection = static_cast<EMovementDirection>(DirectionNum);
}
*/
void ASGSnakeBase::AddElement(int ElementsNum)
{
    for (int i = 0; i < ElementsNum; ++i)
    {
        const auto ElementLocation = FVector(SnakeElements.Num() * DistanceBetween, 0, 0);
        const FTransform ElementSpawnTransform(ElementLocation);
        const auto SnakeElement = GetWorld()->SpawnActor<ASGSnakeElementBase>(SnakeElementClass, ElementSpawnTransform);
        SnakeElement->SnakeOwner = this;
        const int32 ElementIndex = SnakeElements.Add(SnakeElement);
        if (ElementIndex == 0) 
        {
            SnakeElement->SetFirstElementType();
        }
    }
}

void ASGSnakeBase::AddEnergy(float EnergyPoints)
{
    float NewEnergy = Energy + EnergyPoints;
    Energy = FMath::Clamp(NewEnergy, 0.f, MaxEnergy);
}

void ASGSnakeBase::FreezeHunger()
{
    bIsFreezed = true;
    GetWorld()->GetTimerManager().SetTimer(TimeToUnfreezHunger, this ,&ASGSnakeBase::UnFreezeHunger, SecondsToFreezeHunger, false);
}

void ASGSnakeBase::AddScorePoints(int32 Points)
{
    ScorePoints += Points;
}

void ASGSnakeBase::UnFreezeHunger()
{
    bIsFreezed = false;
    GetWorld()->GetTimerManager().ClearTimer(TimeToUnfreezHunger);
}

void ASGSnakeBase::CheckPlaygroundBorders()
{
    if (!GetWorld()->GetFirstPlayerController()) return;
    const auto Pawn = GetWorld()->GetFirstPlayerController()->GetPawnOrSpectator();
    const auto SnakePawn = Cast<ASGPlayerPawnBase>(Pawn);
    if (!SnakePawn) return;
    
    const FVector SnakeLocation = SnakeElements[0]->GetActorLocation();
    const TInterval<float> PlaygroundSpace( - SnakePawn->GetPlaygroundSize() / 2 * DistanceBetween, SnakePawn->GetPlaygroundSize() / 2 * DistanceBetween);
    if (SnakeLocation.X < PlaygroundSpace.Min || SnakeLocation.X > PlaygroundSpace.Max)
    {
        Destroy();
    }
    if (SnakeLocation.Y < PlaygroundSpace.Min || SnakeLocation.Y > PlaygroundSpace.Max)
    {
        Destroy();
    }
}
