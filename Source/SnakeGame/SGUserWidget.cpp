// Fill out your copyright notice in the Description page of Project Settings.

#include "SGUserWidget.h"
#include "SGPlayerPawnBase.h"

float USGUserWidget::GetEnergyPercentage() const
{
    const auto OwningPawn = Cast<ASGPlayerPawnBase>(GetOwningPlayerPawn());
    if (!OwningPawn) return 0.0f;

    return OwningPawn->GetEnergyPercentage();
}

int32 USGUserWidget::GetScorePoints() const
{
    const auto OwningPawn = Cast<ASGPlayerPawnBase>(GetOwningPlayerPawn());
    if (!OwningPawn) return 0;

    return OwningPawn->GetScorePoints();
}
